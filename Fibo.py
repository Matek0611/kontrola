import math

class Fibo:
    def calc(n):
        if n < 1: return 0
        if n == 1: return 1

        return Fibo.calc(n-1) + Fibo.calc(n-2)

    def checkIsItFiboElement(n):
        ispsq = lambda x: int(math.sqrt(x))**2 == x
        
        return ispsq(5*(n**2) + 4) or ispsq(5*(n**2) - 4)
