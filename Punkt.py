import math

class Punkt:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return "P = " + str((self.x, self.y, self.z))

    def distance(self, p):
        return math.sqrt((p.x - self.x)**2 + (p.y - self.y)**2 + (p.z - self.z)**2)


