import string

bl = sorted(string.ascii_uppercase)
sl = sorted(string.ascii_lowercase)

for i in range(len(bl)):
    print(bl[i] if i % 2 == 0 else sl[i], end="")
